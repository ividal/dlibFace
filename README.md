You will need dlib http://dlib.net/ and OpenCV 2.4.

Download http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2 and extract it to the source directory.

Make sure OpenCV_DIR is pointing to the right directory (e.g. the folder where FindOpenCV.cmake resides in opencv) and DLIB_CMAKE to path_to_dlib/cmake.
